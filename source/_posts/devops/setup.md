---
title: Devops and git tricks on docker
category: tricks
tags: [docker, devops, git]
date: "2022-05-20 12:00:01"
---

# Setup project
Here we describe how to start the project
## Dependancies
```s
docker -v
docker-compose -v
git -v
```

## pull project
```s
git clone $PROJECT && cd 
docker-compose pull
docker-compose up -d --build
```