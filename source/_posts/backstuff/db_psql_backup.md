---
title: Database tricks on postgresql
category: tricks
tags: [psql, db, backup, restore]
date: "2022-11-25 12:00:01"
---

# Tricks to backup db
When node is stopping, data might be lost.
## Backup
```s
docker exec -t your-db-container pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
```
## Restore
```s
cat your_dump.sql | docker exec -i your-db-container psql -U postgres
```
