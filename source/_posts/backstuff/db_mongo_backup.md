---
title: Database tricks on mongodb
category: tricks
tags: [mongo, db, backup, restore]
date: "2022-11-25 12:00:01"
---

# Tricks to backup db
## import csv
```s
mongoimport -d database01 -c recipes1000 --type csv --file datatset1000.csv --headerline
```

## Backup
```s
mongodump -d cook
```
## Restore
```s
mongorestore -d cook-recipe-database_1000 recipes1000.bson
```
The --database is reported as deprecated but still works in 2023. Instead namespace pattern should be used, db.collection.


### data restore strategy

#### first policy - small backup
- restore only a small dump at build
    - keep functionnality
    - debloat docker images (2GB full dump)
    - plan on api restore

#### 2 choices
- restore from bson (binary encoded version of JSON) 
- but zip to csv (text) is worthy too. (500mb)

#### more on compression
[quora zip text ppmd / 7z as cmix takes hours](https://qr.ae/pvEPzA)
p7zip / 7z `7z a -m0=PPMd cook.7z full_dataset.csv` 310 from 520 MB