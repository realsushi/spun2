---
title: Poll app documentation usecases example
category: application
tags: [architecture, app, documentation, use-case, requirements, example, tasks, delivery, system-engineering]
date: "2022-05-21 12:00:01"
---

# Project Usecase 
System view of the project
## Requirements US
- REQ100: as a maintainer I want to be able to read web vm status in order to perform monitoring
- REQ200: as a maintainer I want to save database in order to be able to restore it later
- REQ300: as a  maitainer I want to be able to backup apps in order to restore it faster
- REQ400: as a maintainer I want to be sure my connection are not readable in order to improve security

## Tasks
#### tasks from REQ100
- T100 connect playbook with azure vm

- T110: install **node_exporter** on web vm
- T111: install **node_exporter** on db vm
- T112: install **node_exporter** on monitoring side / testing - optionnal

- T120: test network between vm and between networks

- T130: store ssh keys on vault cloud

#### tasks from REQ200
- T200: perform  database backup command
- T210: perform database restore command

#### tasks from REQ300
- T300: perform docker app backup to tar
- T301: perform docker app restore from tar (runtime?)
- T302: perform psql package/config backup/restore
- T303: perform redis package/config backup/restore


- T400: certbots the connections
