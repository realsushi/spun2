---
title: Poll app project architecture example
category: application
tags: [architecture, app, microservices, docker]
date: "2022-05-15 12:00:01"
image: "poll-app-diagram.png"
---

# Architecture
Infrastructure and services description.
2 parts: 1 ensemble split within 2 vms and maintenance side
## Client side
- **WEB** a vm with docker services (docker-compose)
    - poll service: a flask app
    - worker a java jvm v8
    - result a flask app
    - and a traefik servcie
- **DB** a vm with services installed
    - redis app
    - psql app
These vms are reachable through a public ip auth with pem file. Protocol ssh.

TODO: we suggest storing pem fil on vault to get it from API.

REM: network is highly unstable!!!
![app-diagram](poll-app-diagram.png)
## monitoring side
- **monitoring** aws node
    - an aws instance with grafana stack
- **backup** aws node
    - perform db backup action
    - perform container state backups