---
title: a chatGPT review by litteracy
category: ai
tags: [ai, nlp, text, artificial, intelligence, gpt3, word, fraud, chatbot, review]
date: "2022-12-25 13:05:01"
---

# a chatGPT review by litteracy
## What is a chatbot?

## Why chatGPT is different?
ChatGPT relies on underlying technology of GPT3 a subset of Artificial Intelligence known as deep learning. This kind of model is so huge for the time being that it requires extreme hardware to be built on and used. As such there are few datacenters used exclusively for this purpose.

## The writer
Ian Bogost is an important writer at The Atlantic newspaper. This writer is also highly skilled in litterature. Having these kind of culture and skills is necessary to review chatGPT.

## The challenge of nowadays tech on AI
While the tech is solid and existing, it is not reliable. It is very possible to build powerful AI but most of the time it is working as a decaying blackbox. This is because AI are highly vulnerable to general bias. This is a synonim of naive. The AI is not able to understand the context of the conversation and will be biased by the data it has been trained on. While this is already a precarious situation, some AI also use on the road data to "improve" or behave better over time. This makes the AI completely defenseless against trolling of fraud. For example driving AI used in semi-autonomous cars are not using gathered data on the field as is. The data is collected but first requires processing.

## How AI shines or how to leverage the use of AI
Without a deeper context understanding, AI is a bit naive and servile, it would be excellent at copying works of other. Like an inspiration source. Filling the void in some sort. Although one would be careful to keep using it on generic content. The more specific the work the more likely a bias or a fraud might slide in. Also it raises unresolved question among human community: it is not commonly admitted to to use an AI at scale. So it is advisable to use it on generic content, easy to process. Using AI without a loop of control, is like launching a rocket for the very first time, a bit dangerous on the side.

## The amazing fraudulent awarness of chatGPT
The fun thing about chatGPT is its awarness of being a scam artist, it will admit on the fly that it is a fraud...

###### reference
[atlantic review of chatGPT](https://www.theatlantic.com/technology/archive/2022/12/chatgpt-openai-artificial-intelligence-writing-ethics/672386/)