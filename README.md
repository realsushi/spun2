# Static website generator
Welcome to my static site generator (among one of them)

## content
There are project specific doc and
[hexo generic doc](./hexo-js-README.md)

## Theme
theme fluid, nice css animation

## Project CI/CD
- CI?

- Regarding CD there are few solutions from the end:
    - gitlab / gihub hosting
    - deploy runner gitlab-ci or deploy from ssh locally with docker

## Challenges
### Content wise
- Create interesting content
- The idea is to unload the bookmark and build content
- need to implement i8n
- only posts done for now
- assets are stored in a mixed bag with common path

### Tech wise
- Hexo being popular in China, some documentation is not readable.
- website hosting can be interesting as a simpler example than a full website
- API ? not so much or has to be dealt at CI/CD stage
- git is done on 1 branch due to being a solo project and deploy process is fast - but more testing can be done on a second branch to avoid polute the website (empty but works)
- js is known to be ok in terms of speed, while go is said to have the wow factor!
- filter chinese chars with `([^\x00-\x7A])+` or `^# ([^\x00-\x7A])+`

## Dev cycle wip stuff

checking custom CD solution [yanboyang](https://www.yanboyang.com/hexowithdocker/)

```s
npm install hexo-cli -g
npm install
hexo generate
hexo serve
```

installed with `brew install hexo` - node v19

### todo:
- change banner
- fix length gone 'words & minutes'
- add lang changer